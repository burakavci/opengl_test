use gl::{
    self,
    types::{GLboolean, GLenum},
};
use sdl2::video::GLContext;
use std::ffi::{CStr, CString};
use std::{self, ffi::c_void};

use crate::gl_array_extension::CPtr;

pub struct GLBuffer {
    pub id: u32,
    buffer_type: GLenum,
}

pub struct GLVertexArrayObject {
    pub id: u32,
}

impl GLVertexArrayObject {
    pub fn bind_buffer_data(
        &self,
        buffer: &GLBuffer,
        index: u32,
        size: i32,
        data_type: GLenum,
        normalized: GLboolean,
        stride: i32,
        first_ptr: *const c_void,
    ) {
        unsafe {
            gl::BindVertexArray(self.id);
            gl::BindBuffer(buffer.buffer_type, buffer.id);
            gl::VertexAttribPointer(
                index, size, data_type, normalized,
                stride, // stride (byte offset between consecutive attributes)
                first_ptr,
            ); // offset of the first component
            gl::EnableVertexAttribArray(index);
            gl::BindVertexArray(0);
            gl::BindBuffer(buffer.buffer_type, 0);
        }
    }
    pub fn bind_buffer_only(&self, buffer: &GLBuffer) {
        unsafe {
            gl::BindVertexArray(self.id);
            gl::BindBuffer(buffer.buffer_type, buffer.id);
            gl::BindVertexArray(0);
            gl::BindBuffer(buffer.buffer_type, 0);
        }
    }
}

impl GLBuffer {
    pub fn set_data<T>(&self, array: &[T], usage: GLenum) {
        unsafe {
            gl::BindBuffer(self.buffer_type, self.id);
            gl::BufferData(
                self.buffer_type,     // target
                array.size_of(),      // size of data in bytes
                array.as_const_ptr(), // pointer to data
                usage,                // usage
            );
            gl::BindBuffer(self.buffer_type, 0); // unbind the buffer
        }
    }
}

pub trait GLContextExtensions {
    fn new_buffer(&self, buffer_type: GLenum) -> GLBuffer;
    fn new_VAO(&self) -> GLVertexArrayObject;
}

impl GLContextExtensions for GLContext {
    fn new_buffer(&self, buffer_type: GLenum) -> GLBuffer {
        let mut buffer = GLBuffer { id: 0, buffer_type };
        unsafe {
            gl::GenBuffers(1, &mut buffer.id);
        }
        buffer
    }

    fn new_VAO(&self) -> GLVertexArrayObject {
        let mut vao = GLVertexArrayObject { id: 0 };
        unsafe {
            gl::GenVertexArrays(1, &mut vao.id);
        }
        vao
    }
}

fn shader_from_source(source: &CStr, kind: gl::types::GLuint) -> Result<gl::types::GLuint, String> {
    let id = unsafe { gl::CreateShader(kind) };
    unsafe {
        gl::ShaderSource(id, 1, &source.as_ptr(), std::ptr::null());
        gl::CompileShader(id);
    };
    let mut success: gl::types::GLint = 1;
    unsafe {
        gl::GetShaderiv(id, gl::COMPILE_STATUS, &mut success);
    };
    if success == 0 {
        let mut len: gl::types::GLint = 0;
        unsafe {
            gl::GetShaderiv(id, gl::INFO_LOG_LENGTH, &mut len);
        }
        let mut buffer: Vec<u8> = Vec::with_capacity(len as usize + 1);
        buffer.extend([b' '].iter().cycle().take(len as usize));
        let error = create_whitespace_cstring_with_len(len as usize);
        unsafe {
            gl::GetShaderInfoLog(
                id,
                len,
                std::ptr::null_mut(),
                error.as_ptr() as *mut gl::types::GLchar,
            );
        }
        return Err(error.to_string_lossy().into_owned());
    }

    Ok(id)
}

fn create_whitespace_cstring_with_len(len: usize) -> CString {
    // allocate buffer of correct size
    let mut buffer: Vec<u8> = Vec::with_capacity(len + 1);
    // fill it with len spaces
    buffer.extend([b' '].iter().cycle().take(len));
    // convert buffer to CString
    unsafe { CString::from_vec_unchecked(buffer) }
}

pub struct Shader {
    pub id: gl::types::GLuint,
}

impl Shader {
    pub fn from_source(source: &CStr, kind: gl::types::GLenum) -> Result<Shader, String> {
        let id = shader_from_source(source, kind)?;
        Ok(Shader { id })
    }

    pub fn from_vert_source(source: &CStr) -> Result<Shader, String> {
        Shader::from_source(source, gl::VERTEX_SHADER)
    }

    pub fn from_frag_source(source: &CStr) -> Result<Shader, String> {
        Shader::from_source(source, gl::FRAGMENT_SHADER)
    }
}

impl Drop for Shader {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteShader(self.id);
        }
    }
}

pub struct Program {
    pub id: gl::types::GLuint,
}

impl Program {
    pub fn from_shaders(shaders: &[Shader]) -> Result<Program, String> {
        let program_id = unsafe { gl::CreateProgram() };

        for shader in shaders {
            unsafe {
                gl::AttachShader(program_id, shader.id);
            }
        }

        unsafe {
            gl::LinkProgram(program_id);
        }

        // continue with error handling here

        for shader in shaders {
            unsafe {
                gl::DetachShader(program_id, shader.id);
            }
        }

        let mut success: gl::types::GLint = 1;
        unsafe {
            gl::GetProgramiv(program_id, gl::LINK_STATUS, &mut success);
        }

        if success == 0 {
            let mut len: gl::types::GLint = 0;
            unsafe {
                gl::GetProgramiv(program_id, gl::INFO_LOG_LENGTH, &mut len);
            }

            let error = create_whitespace_cstring_with_len(len as usize);

            unsafe {
                gl::GetProgramInfoLog(
                    program_id,
                    len,
                    std::ptr::null_mut(),
                    error.as_ptr() as *mut gl::types::GLchar,
                );
            }

            return Err(error.to_string_lossy().into_owned());
        }

        Ok(Program { id: program_id })
    }

    pub fn use_self(&self) {
        unsafe {
            gl::UseProgram(self.id);
        }
    }
}

impl Drop for Program {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteProgram(self.id);
        }
    }
}
